﻿using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UserRegistration.Consumer
{
    public class ConsoleConsumer : RabbitMqClientBase, IHostedService
    {
        protected string QueueName { get; } = "UserRegistration.console.message";

        public ConsoleConsumer()
        {
            try
            {
                var consumer = new EventingBasicConsumer(Channel);
                consumer.Received += (sender, e) =>
                {
                    var body = e.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(message);
                };
                Channel.BasicConsume(queue: QueueName, autoAck: false, consumer: consumer);
                Console.WriteLine("Consumer started");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error while consuming message, {ex}");
            }
        }

        public virtual Task StartAsync(CancellationToken cancellationToken) => Task.CompletedTask;

        public virtual Task StopAsync(CancellationToken cancellationToken)
        {
            Dispose();
            return Task.CompletedTask;
        }
    }
}
