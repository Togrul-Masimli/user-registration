﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserRegistration.Consumer
{
    public abstract class RabbitMqClientBase : IDisposable
    {
        protected const string VirtualHost = "UserRegistration";
        protected readonly string ConsoleExchange = $"{VirtualHost}.ConsoleExchange";
        protected readonly string ConsoleQueue = $"{VirtualHost}.console.message";
        protected const string ConsoleQueueAndExchangeRoutingKey = "console.message";

        protected IModel Channel { get; private set; }
        private IConnection _connection;

        protected RabbitMqClientBase()
        {
            ConnectToRabbitMq();
        }

        private void ConnectToRabbitMq()
        {
            ConnectionFactory factory = new ConnectionFactory();
            var uri = new Uri("amqp://guest:guest@rabbit:5672/UserRegistration");
            _connection = factory.CreateConnection();

            if (Channel == null || Channel.IsOpen == false)
            {
                Channel = _connection.CreateModel();
                Channel.ExchangeDeclare(
                    exchange: ConsoleExchange,
                    type: "direct",
                    durable: true,
                    autoDelete: false);

                Channel.QueueDeclare(
                    queue: ConsoleQueue,
                    durable: false,
                    exclusive: false,
                    autoDelete: false);

                Channel.QueueBind(
                    queue: ConsoleQueue,
                    exchange: ConsoleExchange,
                    routingKey: ConsoleQueueAndExchangeRoutingKey);
            }
        }

        public void Dispose()
        {
            try
            {
                Channel?.Close();
                Channel?.Dispose();
                Channel = null;

                _connection?.Close();
                _connection?.Dispose();
                _connection = null;
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot dispose RabbitMQ channel or connection, {ex}");
            }
        }
    }
}
