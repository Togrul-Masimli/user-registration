﻿using Core.Entities;
using Core.IoC;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Core.DataAccess.ADO.NET
{
    public class AdoNetEntityRepositoryBase<TEntity> : IEntityRepository<TEntity>
        where TEntity : class, IEntity, new()
    {
        public List<TEntity> GetAll(string procedureName)
        {
            var configuration = ServiceTool.ServiceProvider.GetService<IConfiguration>();

            var connectionString = configuration.GetConnectionString("PostgreConnectionString") 
                ?? throw new Exception("You have sent a blank connection string! Something went wrong. Please try again.");

            NpgsqlConnection connection = new NpgsqlConnection(connectionString);

            connection.Open();

            List<TEntity> result = new List<TEntity>();

            try
            {
                NpgsqlCommand command = new NpgsqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = procedureName;
                var reader = command.ExecuteScalar();

                result = JsonConvert.DeserializeObject<List<TEntity>>(reader.ToString());

                connection.Close();
                connection.Dispose();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw new Exception(ex.Message);
            }

            return result;
        }

        
    }
}
