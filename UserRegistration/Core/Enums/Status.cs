﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Enums
{
    public enum Status : byte
    {
        Active = 1,
        Canceled = 2,
        Deleted = 3
    }
}
