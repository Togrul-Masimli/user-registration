﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.MessageBrokers.RabbitMQ
{
    public interface IRabbitMqProducer<in T>
    {
        void Publish(T @event);
    }
}
