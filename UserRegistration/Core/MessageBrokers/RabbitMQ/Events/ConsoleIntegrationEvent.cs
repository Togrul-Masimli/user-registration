﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.MessageBrokers.RabbitMQ.Events
{
    public class ConsoleIntegrationEvent
    {
        public string Message { get; set; }
    }
}
