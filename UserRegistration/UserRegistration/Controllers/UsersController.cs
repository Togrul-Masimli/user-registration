﻿using Business.Abstract;
using Entites.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserRegistration.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("users")]
        public IActionResult GetAll()
        {
            var result = _userService.GetAll();

            return Ok(result);
        }

        [HttpGet("users-prez")]
        public IActionResult GetAllPrez()
        {
            var result = _userService.GetAllPrez();

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            var result = _userService.Create(user);

            return Ok(result);
        }

        [HttpDelete("users/{id}")]
        public IActionResult Delete(int id)
        {
            var result = _userService.Delete(id);

            return Ok(result);
        }
    }
}
