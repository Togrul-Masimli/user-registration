﻿using Core.DataAccess;
using Entites.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Abstract
{
    public interface IUserDal : IEntityRepository<User>
    {
        void Create(User user);
        void Delete(int id);
        bool CheckEmailUniqueness(string email);
    }
}
