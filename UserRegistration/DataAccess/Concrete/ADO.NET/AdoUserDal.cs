﻿using Core.DataAccess.ADO.NET;
using Core.IoC;
using DataAccess.Abstract;
using Entites.Concrete;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataAccess.Concrete.ADO.NET
{
    public class AdoUserDal : AdoNetEntityRepositoryBase<User>, IUserDal
    {
        IConfiguration configuration = ServiceTool.ServiceProvider.GetService<IConfiguration>();

        public bool CheckEmailUniqueness(string email)
        {
            var connectionString = configuration.GetConnectionString("PostgreConnectionString")
                            ?? throw new Exception("You have sent a blank connection string! Something went wrong. Please try again.");

            NpgsqlConnection connection = new NpgsqlConnection(connectionString);

            connection.Open();

            bool result;

            try
            {
                NpgsqlCommand command = new NpgsqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "public.user_check_email_uniqueness";
                command.Parameters.AddWithValue("_email", email);
                var reader = command.ExecuteScalar();

                result = Convert.ToBoolean(reader);

                connection.Close();
                connection.Dispose();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw new Exception(ex.Message);
            }

            return result;

        }

        public void Create(User user)
        {
            var connectionString = configuration.GetConnectionString("PostgreConnectionString")
                ?? throw new Exception("You have sent a blank connection string! Something went wrong. Please try again.");

            NpgsqlConnection connection = new NpgsqlConnection(connectionString);

            connection.Open();

            try
            {
                NpgsqlCommand command = new NpgsqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "public.user_insert";
                command.Parameters.AddWithValue("_firstname", user.firstname);
                command.Parameters.AddWithValue("_lastname", user.lastname);
                command.Parameters.AddWithValue("_email", user.email);
                command.Parameters.AddWithValue("_password", user.password);
                var reader = command.ExecuteScalar();

                connection.Close();
                connection.Dispose();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw new Exception(ex.Message);
            }

        }

        public void Delete(int id)
        {
            var connectionString = configuration.GetConnectionString("PostgreConnectionString")
                ?? throw new Exception("You have sent a blank connection string! Something went wrong. Please try again.");

            NpgsqlConnection connection = new NpgsqlConnection(connectionString);

            connection.Open();

            try
            {
                NpgsqlCommand command = new NpgsqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "public.user_delete";
                command.Parameters.AddWithValue("_id", id);
                var reader = command.ExecuteScalar();

                connection.Close();
                connection.Dispose();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw new Exception(ex.Message);
            }
        }
    }
}
