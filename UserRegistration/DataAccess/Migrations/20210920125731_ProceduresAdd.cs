﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class ProceduresAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"create or replace function user_select()
                returns json as
                $$
                declare
                result json := '{}';
                sql text := '';
                begin
	                sql :='select array_to_json(array_agg(row_to_json(t))) from (select * from users) t';
	
	                execute sql into result;
	                return result;
                end 
                $$
                language plpgsql;");

            migrationBuilder.Sql(
                @"create or replace function user_insert(_firstname varchar, _lastname varchar, _email varchar, _password text)
                returns int as
                    $$
                    begin
                        insert into users values (default, _firstname, _lastname, _email, _password);
                        if found then
                            return 1;
                        else return 0;
                        end if;
                    end;
                    $$
                language plpgsql;");

            migrationBuilder.Sql(
                @"create or replace function user_check_email_uniqueness(_email text)
                returns boolean as
                    $$
                    declare
                        result int := 0;
                    begin
                        execute 'select count(email) from users u where u.email = ''' || _email || '''' into result;

                        if (result > 0) then
                            return true;
                        else
                            return false;
                        end if;

                    end
                    $$
                language plpgsql;");

            migrationBuilder.Sql(
                @"create or replace function user_delete(_id int)
                returns int as
                    $$
                    begin
                        delete from users where id = _id;
                        if found then
                            return 1;
                        else return 0;
                        end if;
                    end;
                    $$
                language plpgsql;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
