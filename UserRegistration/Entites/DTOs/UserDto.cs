﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entites.DTOs
{
    public class UserDto : IDto
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
