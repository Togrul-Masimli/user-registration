﻿using Core.Entities;
using Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entites.Concrete
{
    public class User : IEntity
    {
        public int id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}
