﻿using AutoMapper;
using Business.Abstract;
using Business.Constants;
using Business.MessageBrokers.Producers;
using Business.ValidationRules.FluentValidation;
using Core.Aspects.Autofac.Validation;
using Core.MessageBrokers.RabbitMQ.Events;
using Core.Utilities.Results;
using DataAccess.Abstract;
using DataAccess.Concrete;
using Entites.Concrete;
using Entites.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Concrete
{
    public class UserManager : IUserService
    {
        private IUserDal _userDal;
        private IMapper _mapper;
        private IProducer _producer;
        public UserManager(IUserDal userDal, IMapper mapper, IProducer producer)
        {
            _userDal = userDal;
            _mapper = mapper;
            _producer = producer;
        }

        [ValidationAspect(typeof(UserValidator))]
        public IResult Create(User user)
        {
            if (user.email == "example@gmail.com")
            {
                throw new Exception("We don't support this email");
            }

            var result = _userDal.CheckEmailUniqueness(user.email);

            if (result)
            {
                return new ErrorResult(Messages.EmailAlreadyExists);
            }

            _userDal.Create(user);
            _producer.Publish(new ConsoleIntegrationEvent { Message = $"User created at {DateTime.Now}!" });

            return new SuccessResult(Messages.UserCreated);
        }

        public IResult Delete(int id)
        {
            _userDal.Delete(id);
            _producer.Publish(new ConsoleIntegrationEvent { Message = $"User deleted at {DateTime.Now}!" });

            return new SuccessResult(Messages.UserDeleted);
        }

        public IDataResult<List<User>> GetAll()
        {
            return new SuccessDataResult<List<User>>(_userDal.GetAll("public.user_select"));
        }

        public IDataResult<List<UserDto>> GetAllPrez()
        {
            var result = _userDal.GetAll("public.user_select");

            List<UserDto> mappedResult = new List<UserDto>();

            foreach (var item in result)
            {
                var mapResult = _mapper.Map<UserDto>(item);
                mappedResult.Add(mapResult);
            }

            return new SuccessDataResult<List<UserDto>>(mappedResult);
        }
    }
}
