﻿using Core.MessageBrokers.RabbitMQ.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.MessageBrokers.Producers
{
    public interface IProducer
    {
        void Publish(ConsoleIntegrationEvent @event);
    }
}
