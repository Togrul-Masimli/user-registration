﻿using Core.MessageBrokers.RabbitMQ.Events;
using Core.MessageBrokers.RabbitMQ.Producers;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.MessageBrokers.Producers
{
    public class ConsoleProducer : ProducerBase<ConsoleIntegrationEvent>, IProducer
    {
        public ConsoleProducer()
        {
        }

        protected override string ExchangeName => "UserRegistration.ConsoleExchange";
        protected override string RoutingKeyName => "console.message";
        protected override string AppId => "ConsoleProducer";

        
    }
}
