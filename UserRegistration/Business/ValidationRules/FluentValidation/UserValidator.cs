﻿using Entites.Concrete;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.ValidationRules.FluentValidation
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(u => u.email).NotEmpty();
            RuleFor(u => u.email).EmailAddress();
            RuleFor(u => u.email).MaximumLength(50);
            RuleFor(u => u.firstname).NotEmpty();
            RuleFor(u => u.firstname).MaximumLength(20);
            RuleFor(u => u.lastname).NotEmpty();
            RuleFor(u => u.lastname).MaximumLength(20);
        }
    }
}
