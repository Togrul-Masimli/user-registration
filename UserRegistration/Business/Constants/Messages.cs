﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Constants
{
    public static class Messages
    {
        public static string EmailAlreadyExists = "This email is used by another user.Please try another one.";
        public static string UserCreated = "User Created Successfully";
        public static string UserDeleted = "User Deleted Successfully";
    }
}
